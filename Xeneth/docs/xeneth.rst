===================
 Xeneth cameras
===================

=============== ========================================================================================================
**Summary**:    :pluginsummary:`Xeneth`
**Type**:       :plugintype:`Xeneth`
**License**:    :pluginlicense:`Xeneth`
**Platforms**:  Windows, Linux
**Devices**:    Xeneth cameras, family Bobcat
**Author**:     :pluginauthor:`Xeneth`
=============== ========================================================================================================
 
Overview
========

.. pluginsummaryextended::
    :plugin: Xeneth

Initialization
==============
  
The following parameters are mandatory or optional for initializing an instance of this plugin:
    
    .. plugininitparams::
        :plugin: Xeneth
